# Matrix General Sources

* [typokign\/matrix\-chart\: Helm chart for deploying a Matrix homeserver stack](https://github.com/typokign/matrix-chart "typokign/matrix-chart: Helm chart for deploying a Matrix homeserver stack")
* [Deploy your own Matrix server on Fedora CoreOS \- Fedora Magazine](https://fedoramagazine.org/deploy-your-own-matrix-server-on-fedora-coreos/ "Deploy your own Matrix server on Fedora CoreOS - Fedora Magazine")
* [Matrix\.org \- Migrating from EMS to self\-hosted Matrix](https://matrix.org/blog/2024/01/migrating-from-ems-to-selfhosted-matrix/ "Matrix.org - Migrating from EMS to self-hosted Matrix")
* [spantaleev\/matrix\-docker\-ansible\-deploy\: 🐳 Matrix \(An open network for secure\, decentralized communication\) server setup using Ansible and Docker](https://github.com/spantaleev/matrix-docker-ansible-deploy/tree/master "spantaleev/matrix-docker-ansible-deploy: 🐳 Matrix \(An open network for secure, decentralized communication\) server setup using Ansible and Docker")
* [Synapse \(Matrix\) VPS requirements \: r\/selfhosted](https://www.reddit.com/r/selfhosted/comments/a3lb13/synapse_matrix_vps_requirements/ "Synapse \(Matrix\) VPS requirements : r/selfhosted")
* [Setup Matrix Synapse Home\-server\. Complete Beginner’s Guide to set\-up… \| by Somnath Das \| Medium](https://medium.com/@dassomnath/setup-matrix-synapse-home-server-ba54e20f8290 "Setup Matrix Synapse Home-server. Complete Beginner’s Guide to set-up… | by Somnath Das | Medium")
* [matrix\-docker\-ansible\-deploy\/docs\/faq\.md at master · spantaleev\/matrix\-docker\-ansible\-deploy](https://github.com/spantaleev/matrix-docker-ansible-deploy/blob/master/docs/faq.md#how-do-i-optimize-this-setup-for-a-low-power-server "matrix-docker-ansible-deploy/docs/faq.md at master · spantaleev/matrix-docker-ansible-deploy")
* [Configuration Manual \- Synapse](https://matrix-org.github.io/synapse/latest/usage/configuration/config_documentation.html#rc_invites "Configuration Manual - Synapse")
