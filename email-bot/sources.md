# Email Bot Development Sources

* [How to Update NPM Dependencies](https://www.freecodecamp.org/news/how-to-update-npm-dependencies/ "How to Update NPM Dependencies")
* [synapse\/docs\/admin\_api\/user\_admin\_api\.md at develop · element\-hq\/synapse](https://github.com/element-hq/synapse/blob/develop/docs/admin_api/user_admin_api.md "synapse/docs/admin_api/user_admin_api.md at develop · element-hq/synapse")
* [Handle plaintext only mails\, fixes \#55 by rda0 · Pull Request \#74 · t2bot\/matrix\-email\-bot](https://github.com/t2bot/matrix-email-bot/pull/74 "Handle plaintext only mails, fixes #55 by rda0 · Pull Request #74 · t2bot/matrix-email-bot")
* [Add address search in Received headers by rda0 · Pull Request \#73 · t2bot\/matrix\-email\-bot](https://github.com/t2bot/matrix-email-bot/pull/73 "Add address search in Received headers by rda0 · Pull Request #73 · t2bot/matrix-email-bot")
* [node\.js \- How can I update each dependency in package\.json to the latest version\? \- Stack Overflow](https://stackoverflow.com/questions/16073603/how-can-i-update-each-dependency-in-package-json-to-the-latest-version "node.js - How can I update each dependency in package.json to the latest version? - Stack Overflow")
* [How to send and receive email in Node \| Basedash](https://www.basedash.com/blog/how-to-send-and-receive-email-in-node "How to send and receive email in Node | Basedash")
* [\@umpacken\/node\-mailin \- npm](https://www.npmjs.com/package/@umpacken/node-mailin "@umpacken/node-mailin - npm")
* [node\-mailin\/lib\/node\-mailin\.js at master · nordluf\/node\-mailin](https://github.com/nordluf/node-mailin/blob/master/lib/node-mailin.js "node-mailin/lib/node-mailin.js at master · nordluf/node-mailin")
* [\@razee\/request\-util \- npm](https://www.npmjs.com/package/@razee/request-util "@razee/request-util - npm")
* [matrix\-bot\-sdk\/src\/MatrixClient\.ts at v0\.7\.1 · turt2live\/matrix\-bot\-sdk](https://github.com/turt2live/matrix-bot-sdk/blob/v0.7.1/src/MatrixClient.ts "matrix-bot-sdk/src/MatrixClient.ts at v0.7.1 · turt2live/matrix-bot-sdk")
* [models\/MatrixError\.ts](https://turt2live.github.io/matrix-bot-sdk/models_MatrixError.ts.html "models/MatrixError.ts")
* [bee\-queue\/bee\-queue\: A simple\, fast\, robust job\/task queue for Node\.js\, backed by Redis\.](https://github.com/bee-queue/bee-queue?tab=readme-ov-file "bee-queue/bee-queue: A simple, fast, robust job/task queue for Node.js, backed by Redis.")
